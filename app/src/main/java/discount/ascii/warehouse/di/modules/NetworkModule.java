package discount.ascii.warehouse.di.modules;

import android.app.Application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import discount.ascii.warehouse.network.Api;
import discount.ascii.warehouse.DiscountApplication;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Julius.
 */
@Module
public class NetworkModule {
    final String baseUrl;

    public NetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    /**
     *Creates interceptor for printing out network requests related logs.
     */
    HttpLoggingInterceptor providesHttpLogging() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    /**
     * Creates cache used to save request data.
     * TODO: calculate size dynamically
     */
    Cache providesCache(Application application) {
        return new Cache(application.getCacheDir(), 10 * 1024 * 1024);
    }

    @Provides
    /**
     * Creates interceptor for caching data, and using cached data.
     */
    Interceptor providesCacheInterceptor() {
        return chain -> {
            Request request = chain.request();
            if (DiscountApplication.isNetworkAvailable()) {
                request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60 * 60).build();
            } else {
                request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60).build();
            }
            return chain.proceed(request);
        };
    }

    @Provides
    /**
     * Creates OkHttpClient used to intercept, and cache requests.
     */
    OkHttpClient providesOkHttp(HttpLoggingInterceptor interceptor, Cache cache,
                                Interceptor cacheInterceptor, @Named("convert") Interceptor convertInterceptor) {
        return new OkHttpClient
                .Builder()
                .cache(cache)
                .addInterceptor(cacheInterceptor)
                .addInterceptor(convertInterceptor)
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    /**
     * Creates Retrofit client.
     */
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Named("convert")
    /**
     * Creates interceptor which should transform ND JSON data to JSON data.
     */
    Interceptor providesConvertInterceptor() {
        return chain -> {
            Request request = chain.request();
            Response response = chain.proceed(request);
            String bodyString = response.body().string();
            String[] lines = bodyString.split(System.getProperty("line.separator"));
            JSONArray jsonArray = new JSONArray();

            for (String line : lines) {
                if (!line.isEmpty()) {
                    try {
                        JSONObject jsonObject = new JSONObject(line);
                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), jsonArray.toString()))
                    .build();
        };
    }

    @Provides Api providesApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

}
