package discount.ascii.warehouse.di.modules;

import android.app.Application;
import android.support.v7.widget.GridLayoutManager;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import discount.ascii.warehouse.R;
import discount.ascii.warehouse.lists.decorators.RecyclerSpacingDecoration;

/**
 * Created by Julius.
 */
@Module
public class RecyclerModule {

    @Provides GridLayoutManager providesGridLayoutManager(Application application) {
        return new GridLayoutManager(application, application.getResources().getInteger(R.integer.sales_grid_columns));
    }

    @Named("grid")
    @Provides RecyclerSpacingDecoration providesSpacingDecoration(Application application) {
        return new RecyclerSpacingDecoration(application.getResources().getDimensionPixelSize(R.dimen.grid_spacing));
    }
}
