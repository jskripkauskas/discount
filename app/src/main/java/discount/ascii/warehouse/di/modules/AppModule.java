package discount.ascii.warehouse.di.modules;

import android.app.Application;

import dagger.Module;
import dagger.Provides;
import discount.ascii.warehouse.DiscountApplication;

/**
 * Created by Julius.
 */
@Module
public class AppModule {
    private final DiscountApplication discountApplication;

    public AppModule(DiscountApplication discountApplication) {
        this.discountApplication = discountApplication;
    }

    @Provides
    Application provideAppContext() {
        return discountApplication;
    }

}
