package discount.ascii.warehouse.di.modules;

import android.app.Application;

import dagger.Module;
import dagger.Provides;
import discount.ascii.warehouse.lists.adapters.DiscountsAdapter;

/**
 * Created by Julius.
 */
@Module
public class AdapterModule {

    @Provides DiscountsAdapter providesDiscountAdapter(Application application) {
        return new DiscountsAdapter(application);
    }
}
