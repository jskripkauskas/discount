package discount.ascii.warehouse;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import discount.ascii.warehouse.di.modules.AppModule;
import discount.ascii.warehouse.di.modules.NetworkModule;

/**
 * Created by Julius.
 */
public class DiscountApplication extends Application {
    protected static DiscountApplication instance;
    protected AppModule appModule;
    protected NetworkModule networkModule;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static DiscountApplication getInstance() {
        return instance;
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) instance.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public AppModule getAppModule() {
        if (appModule == null) {
            appModule = new AppModule(this);
        }
        return appModule;
    }

    public NetworkModule getNetworkModule(String baseUrl) {
        if (networkModule == null || !networkModule.getBaseUrl().equals(baseUrl)) {
            networkModule = new NetworkModule(baseUrl);
        }
        return networkModule;
    }
}
