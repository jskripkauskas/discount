package discount.ascii.warehouse.network;

import java.util.List;

import discount.ascii.warehouse.models.SalesItem;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Julius.
 */
public interface Api {
    String SERVICE_ENDPOINT = "http://74.50.59.155:5000";

    @GET("api/search")
    Observable<List<SalesItem>> search(@Query("q") String query, @Query("limit") Integer limit,
                                       @Query("skip") Integer skip, @Query("onlyInStock") Boolean inStock);
}
