package discount.ascii.warehouse.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Julius.
 * Sales data object.
 */
public class SalesItem implements Parcelable {
    public String type;
    public String id;
    public int size;
    public int price;
    public String face;
    public int stock;
    public ArrayList<String> tags;

    public View.OnClickListener buyListener = view -> view.post(() -> Toast.makeText(view.getContext(), "Congrats!", Toast.LENGTH_SHORT).show());

    public static final Creator<SalesItem> CREATOR = new Creator<SalesItem>() {
        @Override
        public SalesItem createFromParcel(Parcel in) {
            return new SalesItem(in);
        }

        @Override
        public SalesItem[] newArray(int size) {
            return new SalesItem[size];
        }
    };

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeString(this.id);
        dest.writeInt(this.size);
        dest.writeInt(this.price);
        dest.writeString(this.face);
        dest.writeInt(this.stock);
        dest.writeStringList(this.tags);
    }

    public SalesItem() {
    }

    protected SalesItem(Parcel in) {
        this.type = in.readString();
        this.id = in.readString();
        this.size = in.readInt();
        this.price = in.readInt();
        this.face = in.readString();
        this.stock = in.readInt();
        this.tags = in.createStringArrayList();
    }

}
