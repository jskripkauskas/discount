package discount.ascii.warehouse.activities;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Component;
import discount.ascii.warehouse.BuildConfig;
import discount.ascii.warehouse.di.modules.AdapterModule;
import discount.ascii.warehouse.network.Api;
import discount.ascii.warehouse.di.modules.AppModule;
import discount.ascii.warehouse.DiscountApplication;
import discount.ascii.warehouse.lists.adapters.DiscountsAdapter;
import discount.ascii.warehouse.di.modules.NetworkModule;
import discount.ascii.warehouse.R;
import discount.ascii.warehouse.di.modules.RecyclerModule;
import discount.ascii.warehouse.lists.decorators.RecyclerSpacingDecoration;
import discount.ascii.warehouse.models.SalesItem;
import retrofit2.Retrofit;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Julius.
 * Activity for main/home screen.
 */
public class MainActivity extends AppCompatActivity {
    //----------------VARIABLES-------------------
    //Constants used to save state of certain views/values
    protected static final String FILTER_STATE = "filterState";
    protected static final String SALES_STATE = "salesData";
    protected static final String QUERY_STATE = "queryState";
    //Dependencies
    @Inject protected Api api;
    @Inject protected DiscountsAdapter adapter;
    @Inject protected GridLayoutManager gridLayoutManager;
    @Inject @Named("grid") protected RecyclerSpacingDecoration listDecoration;
    //View binds
    @BindView(R.id.toolbar) protected Toolbar toolbar;
    @BindView(R.id.discountsList) protected RecyclerView discountsList;
    protected CheckBox filterBox;
    //Search related variables
    protected String lastSearch;

    //Dagger component for dependency injection
    @Component(modules = {NetworkModule.class, AppModule.class, AdapterModule.class, RecyclerModule.class})
    public interface MainActivityComponent {
        Application app();

        Retrofit retrofit();

        void inject(MainActivity activity);
    }

    //-----------------STATUS----------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        ArrayList<SalesItem> items = ((DiscountsAdapter) discountsList.getAdapter()).getItems();
        state.putParcelableArrayList(SALES_STATE, items);
        state.putString(QUERY_STATE, lastSearch);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        ArrayList<SalesItem> items = state.getParcelableArrayList(SALES_STATE);
        adapter.clear();
        adapter.addItems(items);
        lastSearch = state.getString(QUERY_STATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        configureFilterMenuItem(menu);
        configureSearchMenuItem(menu);
        return true;
    }

    //-----------------MAIN--------------------

    /**
     * Configures filter view within action bar menu.
     *
     * @param menu Menu within this view exists.
     */
    protected void configureFilterMenuItem(Menu menu) {
        final MenuItem filterItem = menu.findItem(R.id.action_stock);
        filterBox = (CheckBox) filterItem.getActionView();
        filterBox.setPadding(0, 0, getResources().getDimensionPixelSize(R.dimen.filter_padding), 0);
        filterBox.setHint(R.string.only_in_stock);
        final SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        //listener in order to persist checkbox value
        filterBox.setOnCheckedChangeListener((compoundButton, b) -> preferences.edit().putBoolean(FILTER_STATE, b).apply());
        filterBox.setChecked(preferences.getBoolean(FILTER_STATE, false));
    }

    /**
     * Configures search view within action bar menu.
     *
     * @param menu Menu within this view exists.
     */
    protected void configureSearchMenuItem(Menu menu) {
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        final View searchButton = searchView.findViewById(android.support.v7.appcompat.R.id.search_go_btn);
        final SearchView.SearchAutoComplete autoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchView.setSubmitButtonEnabled(true);
        //Listen for keyboard action button/"Done" clicks, send search query when clicked
        autoComplete.setOnEditorActionListener((textView, i, keyEvent) -> {
            queryAndCollapse(searchItem, autoComplete.getText().toString());
            return false;
        });
        //Listen for search button clicks, send search query when clicked
        searchButton.setOnClickListener(view -> queryAndCollapse(searchItem, autoComplete.getText().toString()));
    }

    /**
     * Sends out search query and collapses search view.
     *
     * @param menuItem Menu item of search view to collapse.
     * @param query    Search query to be sent out.
     */
    protected void queryAndCollapse(MenuItem menuItem, String query) {
        search(query);
        menuItem.collapseActionView();
    }

    /**
     * Initialise and setup, views, and variables.
     */
    protected void init() {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.main_title);
        }
        initDependencies();
        setupDiscountList();
    }

    /**
     * Initialise/run dependency injection.
     */
    protected void initDependencies() {
        DiscountApplication application = ((DiscountApplication) getApplication());
        DaggerMainActivity_MainActivityComponent
                .builder()
                .appModule(application.getAppModule())
                .networkModule(application.getNetworkModule(Api.SERVICE_ENDPOINT))
                .build()
                .inject(this);
    }

    /**
     * Sets up discounts list for use later on.
     */
    protected void setupDiscountList() {
        discountsList.setAdapter(adapter);
        discountsList.setLayoutManager(gridLayoutManager);
        discountsList.addItemDecoration(listDecoration);

        discountsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLastItemDisplaying(recyclerView)) {
                    search(lastSearch);
                }
            }
        });
    }

    /**
     * Checks whether last item in a list is fully displayed.
     *
     * @param recyclerView List to check.
     * @return True if displayed, false otherwise.
     */
    protected boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((GridLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sends out search query request.
     *
     * @param query Search query.
     */
    protected void search(final String query) {
        if (query == null || !query.equals(lastSearch)) {
            adapter.clear();
        }
        adapter.setLoading(true);
        adapter.notifyDataSetChanged();

        api.search(query, calculateSearchLimit(), discountsList.getAdapter().getItemCount(), filterBox != null && filterBox.isChecked())
                .debounce(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<SalesItem>>() {
                    @Override public void onCompleted() {
                        adapter.setLoading(false);
                    }

                    @Override public void onError(Throwable e) {
                        if (BuildConfig.DEBUG) {
                            e.printStackTrace();
                        }
                        adapter.setLoading(false);
                    }

                    @Override public void onNext(List<SalesItem> salesItems) {
                        adapter.addItems((ArrayList<SalesItem>) salesItems);
                        lastSearch = query;
                    }
                });
    }

    /**
     * Calculates search limit to request only number of items needed to fill list.
     *
     * @return Search limit.
     */
    protected int calculateSearchLimit() {
        int numberOfColumns = getResources().getInteger(R.integer.sales_grid_columns);
        int listItemHeight = adapter.getItemHeight();
        return listItemHeight > 0 ? discountsList.getHeight() / listItemHeight * numberOfColumns : getResources().getInteger(R.integer.default_limit);
    }
}
