package discount.ascii.warehouse.lists.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import discount.ascii.warehouse.R;
import discount.ascii.warehouse.lists.holders.DiscountHolder;
import discount.ascii.warehouse.models.SalesItem;

/**
 * Created by Julius.
 */
public class DiscountsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int LIST_ITEM = 0;
    private static final int LOAD_ITEM = 1;
    private final ArrayList<SalesItem> data;
    private final Context context;
    private int itemHeight = 0;
    private boolean loading = false;

    public DiscountsAdapter(Context context) {
        this.context = context;
        data = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == LIST_ITEM) {
            view = LayoutInflater.from(context).inflate(R.layout.sales_item_layout, parent, false);
            return new DiscountHolder(view);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.load_item_layout, parent, false);
            return new RecyclerView.ViewHolder(view) {
            };
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position < data.size()) {
            DiscountHolder discountHolder = (DiscountHolder) holder;
            discountHolder.getBinding().setItem(data.get(position));
            discountHolder.itemView.post(() -> itemHeight = holder.itemView.getMeasuredHeight());
        }
    }

    @Override
    public int getItemCount() {
        return data.size() + (loading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size()) {
            return LOAD_ITEM;
        } else {
            return LIST_ITEM;
        }
    }

    /**
     * Adds a collection of sales items.
     *
     * @param data Collection of sales items to be added.
     */
    public void addItems(ArrayList<SalesItem> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    /**
     * Adds single sales item.
     *
     * @param salesItem Sales item to be added.
     */
    public void addItem(SalesItem salesItem) {
        if (!this.data.contains(salesItem)) {
            this.data.add(salesItem);
            notifyItemInserted(data.size() - 1);
        }
    }

    /**
     * Clears current data (does not notify to update).
     */
    public void clear() {
        this.data.clear();
    }

    public ArrayList<SalesItem> getItems() {
        return data;
    }

    public int getItemHeight() {
        return itemHeight;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }
}
