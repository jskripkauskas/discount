package discount.ascii.warehouse.lists.holders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import discount.ascii.warehouse.databinding.SalesItemLayoutBinding;

/**
 * Created by Julius.
 */
public class DiscountHolder extends RecyclerView.ViewHolder {
    private final SalesItemLayoutBinding binding;

    public DiscountHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    public SalesItemLayoutBinding getBinding() {
        return binding;
    }

}
