package discount.ascii.warehouse.di.modules;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import dagger.Provides;
import discount.ascii.warehouse.models.SalesItem;
import discount.ascii.warehouse.network.Api;
import retrofit2.Retrofit;
import rx.Observable;

import static org.mockito.Matchers.any;

/**
 * Created by Julius.
 */
public class NetworkModuleTest extends NetworkModule {

    public NetworkModuleTest(String baseUrl) {
        super(baseUrl);
    }

    @Provides
    @Override
    Api providesApi(Retrofit retrofit) {
        Api api = Mockito.mock(Api.class);
        List<SalesItem> data = new ArrayList<>();
        SalesItem dummyItem = new SalesItem();
        dummyItem.price = 1234;
        data.add(dummyItem);
        //when we search for anything return same result
        Mockito.when(api.search(any(), any(), any(), any())).thenReturn(Observable.just(data));
        return api;
    }

}
