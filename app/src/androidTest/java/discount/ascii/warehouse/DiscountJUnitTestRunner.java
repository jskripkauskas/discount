package discount.ascii.warehouse;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;

public class DiscountJUnitTestRunner extends AndroidJUnitRunner {

    @Override
    public Application newApplication(ClassLoader c1, String className, Context context) throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        return super.newApplication(c1, ApplicationOverrideEspresso.class.getName(), context);
    }
}