package discount.ascii.warehouse;

import android.annotation.SuppressLint;

import discount.ascii.warehouse.di.modules.NetworkModule;
import discount.ascii.warehouse.di.modules.NetworkModuleTest;

@SuppressLint("Registered")
public class ApplicationOverrideEspresso extends DiscountApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public NetworkModule getNetworkModule(String baseUrl) {
        if (networkModule == null || !networkModule.getBaseUrl().equals(baseUrl)) {
            networkModule = new NetworkModuleTest(baseUrl);
        }
        return networkModule;
    }

}